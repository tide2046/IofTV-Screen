/*
 * @Author: daidai
 * @Date: 2022-01-12 14:22:29
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-28 14:53:02
 * @FilePath: \web-pc\src\pages\big-screen\router\index.js
 */
import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [  {
  path: '/',
  redirect: '/index',
},
{
  path: '/home',
  name: 'home',
  component: () => import(/* webpackChunkName: "LSD.bighome" */ '../views/home.vue'),
  children:[
    {
      path: '/index',
      name: 'index',
      component: () => import(/* webpackChunkName: "LSD.bighome" */ '../views/index/index.vue'),
    },
	{
	  path: '/plot',
	  name: 'plot',
	  component: () => import(/* webpackChunkName: "LSD.bighome" */ '../views/plot/index.vue'),
	},
	{
	  path: '/vari',
	  name: 'vari',
	  component: () => import(/* webpackChunkName: "LSD.bighome" */ '../views/vari/index.vue'),
	},
	{
	  path: '/plant',
	  name: 'plant',
	  component: () => import(/* webpackChunkName: "LSD.bighome" */ '../views/plant/index.vue'),
	},
	{
	  path: '/diseasePest',
	  name: 'diseasePest',
	  component: () => import(/* webpackChunkName: "LSD.bighome" */ '../views/diseasePest/index.vue'),
	}
  ]
}, 
];
const router = new VueRouter({
  // mode: "hash",//路由模式及url中#号
  mode: "history",//history模式,url不带#
  // base: process.env.BASE_URL,
  routes
});

export default router;