/*
 * @Author: daidai
 * @Date: 2021-12-23 11:18:37
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-04-28 15:10:45
 * @FilePath: \web-pc\src\api\modules\index.js
 */
import * as API from "../api";

export const paramType = {
	'overview': "/overview/intro", //总览
	'area': "/overview/area", //面积 
	'prod': "/overview/prod", //产量
	'inOutput': "/overview/inOutput", //投入产出
	'impSumPrice': '/overview/impSumPrice', //进口数量及价格  
	'impCountry': '/overview/impCountry', //进口来源国占比  
	'resultRate': '/overview/resultRate', //成果转化率

	'plots': '/plot/plots', //中间地图
	'info': '/plot/info', //地块信息
	'devices': '/plot/devices', //仪器设备
	'weather': '/plot/weather', //气象监测
	'deviceWeather': '/plot/deviceWeather', //气象监测
	'soil': '/plot/soil', //土壤监测
	'images': '/plot/images', //影像监测
	'light': '/plot/light', //光照度光量子监测
	'remote': '/plot/remote', //叶面温湿度监测

	'breed': '/vari/breed', //育成品种
	'period': '/vari/period', // 大豆生长发育图

	'plant': '/plant/tech', // 栽种技术

	'diseasePest': '/diseasePest/info', //气象监测

	'big8': '/bigscreen/centermap', // //中间地图
}
/******************      通用增删改查       ********************* */
/**
 * 通用列表
 * @param {*} param 
 */
export const currentList = (key, param) => {
	return API.GET(paramType[key] + "/list", param)
}
export const currentPage = (key, param) => {
	return API.GET(paramType[key] + "/page", param)
}
/**
 * 查询可选择的列表
 * @param {*} param 
 */
export const currentSelectList = (key, param) => {
	return API.GET(paramType[key] + "/selectList", param)
}


/**
 * 通用新增
 * @param {*} param 
 */
export const currentSave = (key, param) => {
	return API.POST(paramType[key] + "/save", param)
}
/**
 * 通用修改
 * @param {*} param 
 */
export const currentUpdate = (key, param) => {
	return API.POST(paramType[key] + "/update", param)
}

/**
 * 通用删除
 * @param {*} param 
 */
export const currentDelete = (key, param) => {
	return API.POST(paramType[key] + "/delete", param)
}

/**
 * 通用获取所有不分页
 * @param {*} param 
 */
export const currentSelect = (key, param) => {
	return API.GET(paramType[key] + "/select", param)
}

/**
 * 通用GET
 * @param {*} param 
 */
export const currentGET = (key, param) => {
	return API.GET(paramType[key], param)
}
/**
 * 通用POST
 * @param {*} param 
 */
export const currentPOST = (key, param) => {
	return API.POST(paramType[key], param)
}
// 通用接口集合
export const currentApi = {
	currentList,
	currentPage,
	currentSave,
	currentUpdate,
	currentDelete,
	currentSelect,
	currentSelectList,
	currentPOST,
	currentGET
}